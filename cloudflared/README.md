# Cloudflared Tunnel Integration

## Deploy cloudflared on kubernetes

Set the secrets accordingly in `cloudflared.yaml` and apply the manifest.

```sh
kubectl apply -f cloudflared.yaml
```

Scale up the correct environment

```sh
kubectl -n cloudflared scale deployment --replicas=2 -l env=vce1
```

or

```sh
kubectl -n cloudflared scale deployment --replicas=2 -l env=vce2
```
