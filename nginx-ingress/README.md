# Install the nginx-ingress

## Create namespace

```sh
kubectl create ns nginx-ingress
```

## Helm install the nginx-ingress using the provided values.yaml file

```sh
helm -n nginx-ingress install nginx-ingress -f values.yaml nginx-stable/nginx-ingress
```
