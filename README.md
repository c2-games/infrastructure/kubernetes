# Deploying Kubernetes

We are using rancher k3s as our kubernetes distribution. The
[Scoring Engine repo](https://gitlab.com/c2-games/ctf/scoring/scoring-engine)
is used to auto-provision the clusters.

## helpful steps

You probably want to do a few things on the system your managing the cluster from to make your life easier.

### kubectl auto completion

Add autocompletion to your bashrc.

```sh
kubectl completion bash >> ~/.bashrc
```

### Setup helm

Either Install helm with a package manager or download the binary from helm's
[GitHub releases page](https://github.com/helm/helm/releases/). Set `KUBECONFIG`
so helm can find the cluster. If your running helm on the k3s master you can do
following. You probably want to add this to your bashrc:

```sh
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
```

## Kubernetes Infrastructure Documentation

* [K8s LoadBalancer (MetalLB)](docs/metallb.md)
* [Network Overlay(Kube-OVN/Multus)](docs/kube-ovn.md)
* [Prometheus/Grafana Monitoring](docs/prometheus-grafana.md)

### Setup notes

Get secrets from BW or some super-admin user, put in file `all-secrets.yml`

#### Setup All Namespaces

```sh
kubectl apply -f namespaces
```

#### RBAC

```sh
kubectl apply -f user-accounts/rbac
```

#### User accounts

Examples!

```sh
bash create_k8s_user.sh ahogan ctf
bash create_k8s_user.sh basta black-team
bash create_k8s_user.sh bdavis black-team
bash create_k8s_user.sh jvandermaas black-team
bash create_k8s_user.sh lhalbert black-team
bash create_k8s_user.sh mlisi orange-team
bash create_k8s_user.sh shall red-team
bash create_k8s_user.sh sradigan black-team
```

```sh
for user in ahogan basta bdavis jvandermaas lhalbert mlisi shall sradigan; do
    cp /home/sradigan/kubernetes/user-accounts/${user}-k8s.conf /home/${user}/.kube/config
    chown ${user}:${user} /home/${user}/.kube/config
    chmod 0600 /home/${user}/.kube/config
done
```

#### Networking

```sh
# It uses the power of sorting to do
# it all the correct order
kubectl apply -f kube-ovn
```

Double check that the subnets are right

```sh
kubectl get subnets
```

**Note:** If it did it WRONG do this

```sh
kubectl delete -f kube-ovn
```

#### CTF

Update the secrets from template!

```sh
cp ctf/*_secret*.y*ml{.j2,}
vim ctf/*_secret*.y*ml
```

### Scoring

Update the secrets from template!

```sh
cp scoring/*_secret*.y*ml{.j2,}
vim scoring/*_secret*.y*ml
```

Update the config map with the correct vceN and event environment

```sh
vim scoring/scoring_configmap.yaml
```

Apply all secrets and config maps

```sh
kubectl apply -f ctf/ctf_secret.yml -f scoring/scoring_secret.yml -f scoring/scoring_configmap.yaml -f scoring/self-destruct-serviceAccount.yaml
```

Generate check manifests

```sh
python -m venv venv
. ./venv/bin/activate
python -m pip install -r requirements.txt
# This file may need edited to update the check info.
# --start-team will be the lowest team to generate manifests for, and --end-team is the highest
python scoring/gen-checks.py --start-team 0 --end-team 12 --scoring --adversarial
```

```sh
# Sub out check-type with desired type: service or adversarial
kubectl apply -f scoring/internal/check-type
kubectl apply -f scoring/external/check-type
kubectl -n scoring scale deployment --replicas=1 -l perspective=external
kubectl -n scoring scale deployment --replicas=1 -l perspective=internal
```
