# -*- coding: utf-8 -*-
"""Module to generate kubernetes networking manifest."""
import argparse
import sys
from pathlib import Path
from typing import Dict, List

from jinja2 import Template

cwd_path = Path()


def gen_configurations():
    """function to generate subnet configuration dict."""
    vlan_base_id = 100
    subnets_dict = {}

    for team in teams:
        if team == "wan":
            provider = "competition"
            vpc_name = "competition-vpc"
            cidr_block = "172.18.0.0/16"
            net_segment = "172.18"
            vlan_id = 0
            exclude_ips = [
                {
                    "start": f"{net_segment}.0.1",
                    "end": f"{net_segment}.63.255",
                },
                {
                    "start": f"{net_segment}.68.0",
                    "end": f"{net_segment}.255.255",
                },
            ]
            gateway = f"{net_segment}.0.1"
        else:

            provider = "student"
            vpc_name = "student-vpc"
            # Extrapolate third octet from team name
            team_num = int(team[4:])
            third_oct = team_num
            net_segment = f"192.168.{third_oct}"
            cidr_block = f"{net_segment}.0/24"
            # Extrapolate vlan_id from team name
            vlan_id = vlan_base_id + team_num
            exclude_ips = [
                {
                    "start": f"{net_segment}.1",
                    "end": f"{net_segment}.63",
                },
                {
                    "start": f"{net_segment}.128",
                    "end": f"{net_segment}.254",
                },
            ]
            gateway = f"{net_segment}.1"

        # Generate subnet dict entry
        subnets_dict[team] = {
            "vlan": {
                "name": f"vlan{vlan_id}",
                "id": vlan_id,
                "provider": provider,
            },
            "cidr_block": cidr_block,
            "exclude_ips": exclude_ips,
            "gateway": gateway,
            "vpc_name": vpc_name,
            "provider": "attachnet.default.ovn",
        }

    return subnets_dict


def overwrite_file(filename: str):
    """function to overwrite existing file."""
    if Path(cwd_path, filename).exists():
        with open(Path(cwd_path, filename), "w", encoding="utf-8") as file:
            file.write("")


def generate_manifests(subnets_dict: dict, vlan_bool: bool, subnet_bool: bool):
    """function to generate manifests."""
    manifests_dict: Dict[str, List[str]] = {"vlan": [], "subnet": []}

    if vlan_bool:
        with open("vlan_template.yaml.j2", encoding="utf-8") as file:
            vlan_template = Template("".join(file.readlines()))

    if subnet_bool:
        with open("subnet_template.yaml.j2", encoding="utf-8") as file:
            subnet_template = Template(
                "".join(file.readlines()), trim_blocks=True, lstrip_blocks=True
            )

    for subnet, attribute in subnets_dict.items():

        if vlan_bool:
            vlan_manifest = vlan_template.render(
                vlan_name=attribute["vlan"]["name"],
                vlan_id=attribute["vlan"]["id"],
                vlan_provider=attribute["vlan"]["provider"],
                team=subnet,
            )

            manifests_dict["vlan"].append(vlan_manifest)

        if subnet_bool:
            subnet_manifest = subnet_template.render(
                subnet_name=subnet,
                cidr_block=attribute["cidr_block"],
                exclude_ips=attribute["exclude_ips"],
                subnet_gateway=attribute["gateway"],
                vlan_name=attribute["vlan"]["name"],
                vpc_name=attribute["vpc_name"],
                provider=attribute["provider"],
            )

            manifests_dict["subnet"].append(subnet_manifest)

    return manifests_dict


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-w", "--include-wan", default=True, action="store_true"
    )
    parser.add_argument(
        "-s", "--start-team", type=int, default=0, required=True
    )
    parser.add_argument(
        "-e", "--end-team", type=int, default=25, required=True
    )
    parser.add_argument("-V", "--vlan", action="store_true")
    parser.add_argument("-S", "--subnet", action="store_true")
    parser.add_argument(
        "--stdout",
        action="store_true",
        help="Write output to stdout.",
    )
    parser.add_argument(
        "--write-out", action="store_true", help="Write output to file."
    )
    args = parser.parse_args()

    teams = [f"Team{x}" for x in range(args.start_team, args.end_team + 1)]

    if args.include_wan:
        teams.insert(0, "wan")

    subnets = gen_configurations()
    manifests = generate_manifests(subnets, args.vlan, args.subnet)
    if args.vlan:
        if args.write_out:
            overwrite_file("01-vlans.yaml")
            with open(
                Path(cwd_path, "01-vlans.yaml"), "a", encoding="utf-8"
            ) as vlan_file:
                for manifest in manifests["vlan"]:
                    vlan_file.write(manifest)
        if args.stdout:
            _ = [sys.stdout.write(i) for i in manifests["vlan"]]

    if args.subnet:
        if args.write_out:
            overwrite_file("03-subnets.yaml")
            with open(
                Path(cwd_path, "03-subnets.yaml"), "a", encoding="utf-8"
            ) as subnet_file:
                for manifest in manifests["subnet"]:
                    subnet_file.write(manifest)
        if args.stdout:
            _ = [sys.stdout.write(i) for i in manifests["subnet"]]
