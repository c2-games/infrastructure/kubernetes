# Install longhorn

## Create the longhorn namespace

```sh
kubectl create ns longhorn-system
```

## Add the longhorn repo and update

```sh
helm repo add longhorn https://charts.longhorn.io
helm repo update
```

## Install the longhorn helm chart

```sh
helm -n longhorn-system install longhorn longhorn/longhorn -f values.yaml
```

## Create storage classes

```sh
kubectl -n longhorn-system apply -f longhorn-1-replica-storageclass.yaml
kubectl -n longhorn-system apply -f longhorn-3-replica-storageclass.yaml
```
