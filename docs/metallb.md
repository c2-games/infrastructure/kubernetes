# MetalLB Infrastructure

MetalLB is used in BGP mode to advertise routes to the upstream pfSense router.
With this configuration, any service exposed with a LoadBalancer type in the
Kubernetes cluster will be assigned an unused IP in the pre-defined range of
`172.20.0.0/16`, and the route advertised upstream. This allows us to expose
services in the same way that we would using a true cloud LoadBalancer.

## Create the MetalLB namespace

```bash
kubectl create ns metallb
```

## Add the MetalLB helm repo and update

```bash
helm repo add metallb https://metallb.github.io/metallb
helm repo update
```

## Install MetalLB using helm

```bash
helm install metallb metallb/metallb --namespace metallb \
--set configInline.address-pools[0].name=bgp \
--set configInline.address-pools[0].protocol=bgp \
--set configInline.address-pools[0].addresses[0]=172.20.0.0/16 \
--set configInline.address-pools[0].avoid-buggy-ips=false \
--set configInline.peers[0].peer-asn=64512 \
--set configInline.peers[0].my-asn=64512 \
--set configInline.peers[0].peer-address=172.18.0.1
```

## Update the metalLB configMap to secure the BGP communication

Note: Smaller address pools can be provisioned with different names

```yaml
apiVersion: v1
data:
  config: |
    address-pools:
    - addresses:
      - 172.20.0.0/16
      avoid-buggy-ips: false
      name: bgp
      protocol: bgp
      bgp-advertisements:
      - communities:
        - 64512:1
        - no-export
    bgp-communities:
      no-export: 65535:65281
    peers:
    - my-asn: 64512
      peer-address: 172.18.0.1
      peer-asn: 64512
      password: **REDACTED**
```

## Verify that LoadBalancer type services are being exposed via BGP routes

Notice the external IP of `172.20.X.X`

```bash
$ kubectl get svc -A | grep LoadBalancer
NAMESPACE               NAME                                                 TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                        AGE
monitoring              kube-prometheus-grafana-lb                           LoadBalancer   10.109.235.27    172.20.0.0    3000:30585/TCP                 22h
private-nginx-ingress   private-nginx-ingress-nginx-ingress                  LoadBalancer   10.100.223.16    172.20.0.1    80:32503/TCP,443:31917/TCP     4d3h
```

![MetalLB-BGP-Routes](metallb-bgp-routes.png "MetalLB BGP Routes")
