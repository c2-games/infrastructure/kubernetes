# Kube-OVN Infrastructure

Kube-OVN is used in conjuction with Multus CNI to provide a highly configurable
Kubernetes overlay network to meet the complexity requirements for this competition.
Kube-OVN is configured in such a way that a Pod can live within a logical
kubernetes subnet, but have secondary interfaces within physical VLAN or macvlan
subnets. This allows us to be more flexible as to what networks each individual
Pod has access to at provisioning time.

![kube-ovn-topology](kube-ovn-topology.png "Kube-OVN/Multus CNI Implementation")

## Creating the `teams` VPC

The `teams` VPC provides a logical container within OVN to hold all of of the
team subnet. The VPC can, optionally, be constrained to namespaces.

```yaml
kind: Vpc
apiVersion: kubeovn.io/v1
metadata:
  name: teams
spec:
#  namespaces:
#  - namespace1
```

To view the VPCs present on the cluster, run the command `kubectl get vpcs`.

```bash
$ kubectl get vpcs
NAME          STANDBY   SUBNETS
ovn-cluster   true      ["ovn-default","join"]
teams         true      ["team0","team1"]
```

## Creating a provider network

The provider network allows us to bind kube-ovn to a specific interface across
all (or a subset) of the K8s nodes. This interface is used to expose a VLAN trunk
to the cluster for physical subnet access. Once a provider network is defined,
we can then proceed to define VLANs and subnets.

```yaml
apiVersion: kubeovn.io/v1
kind: ProviderNetwork
metadata:
  name: competition
spec:
  defaultInterface: eth1
#  customInterfaces:
#  - interface: eth2
#    nodes:
#      - node1
#  excludeNodes:
#    - k8s-master01.cnyhackathon.org
```

```bash
$ kubectl get provider-networks.kubeovn.io
NAME          DEFAULTINTERFACE
competition   eth1
```

## Creating VLANs

```yaml
apiVersion: kubeovn.io/v1
kind: Vlan
metadata:
  name: team0-vlan100
spec:
  id: 100
  provider: competition
```

```bash
$ kubectl get vlans.kubeovn.io
NAME            ID    PROVIDER
team0-vlan100   100   competition
team1-vlan101   101   competition
```

## Creating subnets

`team0-subnet.yaml`

```yaml
apiVersion: kubeovn.io/v1
kind: Subnet
metadata:
  name: team0
spec:
  allowSubnets:
  - 192.168.0.0/24
  cidrBlock: 192.168.0.0/24
  default: false
  disableGatewayCheck: true
  disableInterConnection: false
  excludeIps:
  - 192.168.0.1..192.168.0.20
  - 192.168.0.133
  - 192.168.0.201
  - 192.168.0.254
  gateway: 192.168.0.1
  gatewayNode: ""
  gatewayType: distributed
  natOutgoing: false
  private: false
  protocol: IPv4
  underlayGateway: true
  vlan: team0-vlan100
  vpc: teams
status: {}
```

`team0-subnet-patch.yaml`

```yaml
apiVersion: kubeovn.io/v1
kind: Subnet
metadata:
  name: team0
spec:
  allowSubnets:
  - 192.168.0.0/24
  cidrBlock: 192.168.0.0/24
  default: false
  disableGatewayCheck: true
  disableInterConnection: false
  excludeIps:
  - 192.168.0.1..192.168.0.20
  - 192.168.0.133
  - 192.168.0.201
  - 192.168.0.254
  gateway: 192.168.0.1
  gatewayNode: ""
  gatewayType: distributed
  natOutgoing: false
  private: false
  protocol: IPv4
  provider: competition
  underlayGateway: true
  vlan: team0-vlan100
  vpc: teams
status: {}
```

Run `kubectl apply -f team0-subnet.yaml` followed by `kubectl apply -f team0-subnet-patch.yaml`
to avoid a bug in which the subnet is not properly assigned to the competition provider.

```bash
$ kubectl get subnets.kubeovn.io
NAME          PROVIDER          VPC           PROTOCOL   CIDR             PRIVATE   NAT     EXTERNALEGRESSGATEWAY   POLICYROUTINGPRIORITY   POLICYROUTINGTABLEID   DEFAULT   GATEWAYTYPE   V4USED   V4AVAILABLE   V6USED   V6AVAILABLE
join          ovn               ovn-cluster   IPv4       100.64.0.0/16    false     false                                                                          false     distributed   11       65522         0        0
macvlan       macvlan.default   ovn-cluster   IPv4       172.18.0.0/16    false     false                                                                          false     distributed   0        256           0        0
ovn-default   ovn               ovn-cluster   IPv4       10.16.0.0/16     false     true                                                                           true      distributed   16       65517         0        0
team0         competition       teams         IPv4       192.168.0.0/24   false     false                                                                          false     distributed   0        231           0        0
team1         competition       teams         IPv4       192.168.1.0/24   false     false                                                                          false     distributed   0        231           0        0
```

## Deploying Multus CNI

### Creating a Network Attachment Defintion to facilitate multi-NIC pods

```yaml
apiVersion: "k8s.cni.cncf.io/v1"
kind: NetworkAttachmentDefinition
metadata:
  name: team-subnets
  namespace: default
spec:
  config: '{
      "cniVersion": "0.3.0",
      "type": "kube-ovn",
      "server_socket": "/run/openvswitch/kube-ovn-daemon.sock",
      "provider": "team-subnets.default.ovn"
    }'
```

## Deploying a pod within a VLAN subnet

To configure a pod withing a VLAN subnet, add the following annotation to the pod spec.

```yaml
ovn.kubernetes.io/logical_switch: ${subnet_name}
```

## Deploying a pod with multiple NICs

To configure a pod with multiple NICs, add the following annotations to the pod
spec. Note that the primary NIC (aka the default route NIC) will reside within
the `ovn-default` subnet and will route internet bound traffic out the cluster NAT.

```yaml
k8s.v1.cni.cncf.io/networks: default/team-subnets
team-subnets.default.ovn.kubernetes.io/logical_switch: ${subnet_name}
```

As can be seen below, the team1-deploy pod has an interface in the `10.16.0.0/16`
range and another in the `192.168.1.0/24` range. All internet destined traffic
will leave via the `10.16.X.X` interface and be handled by the cluster's default
NAT and egress policies.

```bash
$ kubectl exec -it team1-deploy-5b87d8cc7d-d2dbt -- ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
231: eth0@if232: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1400 qdisc noqueue state UP
    link/ether 00:00:00:7d:c6:4b brd ff:ff:ff:ff:ff:ff
    inet 10.16.0.41/16 brd 10.16.255.255 scope global eth0
       valid_lft forever preferred_lft forever
233: net1@if234: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP
    link/ether 00:00:00:d5:0b:7f brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.24/24 brd 192.168.1.255 scope global net1
       valid_lft forever preferred_lft forever

$ kubectl exec -it team1-deploy-5b87d8cc7d-d2dbt -- ip route
default via 10.16.0.1 dev eth0
10.16.0.0/16 dev eth0 scope link  src 10.16.0.41
192.168.1.0/24 dev net1 scope link  src 192.168.1.24
```

## Deploy a pod with a static IP address

To configure a static IP address, add the following annotation to the pod spec.
Note that the IP address must be within the subnet CIDR.

```yaml
ovn.kubernetes.io/ip_address: a.b.c.d
```
