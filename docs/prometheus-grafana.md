# Prometheus/Grafana Monitoring

Prometheus is used to monitor the health and status of the overall cluster, while
grafana is used to give realtime metrics for most operations taking place on the cluster.

![grafana-kube-ovn-pinger dashboard](grafana-kube-ovn-pinger.png "Grafana Kube-OVN Pinger Dashboard")

## Install Kube-Prometheus-Stack

### Create the monitoring namespace

```bash
kubectl create ns monitoring
```

### Add the Prometheus community helm repo and update

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

### Install the kube-prometheus-stack (previously known as prometheus-operator) via helm

```bash
helm install kube-prometheus prometheus-community/kube-prometheus-stack --namespace monitoring
```

### Expose the grafana interface via a load-balancer

Note: This can also be exposed via an ingress so as to handle TLS termination and
to avoid provisioning multiple LBs for HTTP/HTTPS services.

Notice that the load-balancer-ip was specified when exposing the service. This
is an optional argument, which if left out will cause kubernetes to dynamically
allocate an IP within the load-balancer range.

```bash
kubectl -n monitoring expose deployment kube-prometheus-grafana --name kube-prometheus-grafana-lb --type LoadBalancer --port 3000 --load-balancer-ip 172.20.0.4
```

### Get Grafana default username and password from the grafana secret

Username

```bash
kubectl -n monitoring get secrets kube-prometheus-grafana -o jsonpath='{.data.admin-user}' | base64 -d
```

Password

```bash
kubectl -n monitoring get secrets kube-prometheus-grafana -o jsonpath='{.data.admin-password}' | base64 -d
```

## Add Custom Monitoring and Dashboards

In order to monitor custom services on the cluster, the service must expose a
prometheus compatible metrics endpoint. Once an endpoint (AKA service) has been
provisioned that exposes the metrics API via a ClusterIP, a custom `ServiceMonitor`
can be provisioned to pass all metrics data to prometheus for ingestion.

### Example metrics endpoint spec

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/instance: metallb
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: metallb
    app.kubernetes.io/version: v0.10.2
    helm.sh/chart: metallb-0.10.2
  name: metallb-metrics
  namespace: metallb
spec:
  ports:
  - name: metrics
    port: 7472
    protocol: TCP
    targetPort: 7472
  selector:
    app.kubernetes.io/component: controller
    app.kubernetes.io/instance: metallb
    app.kubernetes.io/name: metallb
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```

### Example ServiceMonitor spec

Note: The label `release: kube-prometheus` is required unless the main
prometheus configMap selector is modified to include other labels.

```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: metallb-monitor
  namespace: monitoring
  labels:
    release: kube-prometheus
spec:
  endpoints:
    - bearerTokenFile: /var/run/secrets/kubernetes.io/serviceaccount/token
      interval: 15s
      port: metrics
  namespaceSelector:
    matchNames:
      - metallb
  selector:
    matchLabels:
      app.kubernetes.io/component: controller
      app.kubernetes.io/instance: metallb
      app.kubernetes.io/name: metallb
```
