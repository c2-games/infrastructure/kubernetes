#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "user and token args must be provided"
    exit 1
fi

user=$1
token=$2
registry_url=$3
secret_name=$4

secret_name=${secret_name:-gitlab-pull-secret}
registry_url=${registry_url:-registry.gitlab.com}


auth_base64=$(echo -n "${user}:${token}" | base64)
pull_secret=$(jq \
    --arg url "${registry_url}" \
    --arg username "${user}" \
    --arg password "${token}" \
    --arg auth "${auth_base64}" \
    '.+ { "auths":{ ($url) :{ username: ($username), password: ($password), auth: ($auth)}}}' <<< '{}')


cat << EOF
kind: Secret
apiVersion: v1
metadata:
  name: ${secret_name}
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: $(echo "${pull_secret}" | base64 | tr -d '\n')
EOF
