
# Install HA PostgreSQL cluster

## Create or reuse an existing namespace

example creation

```sh
kubectl create ns postgres
```

## Helm install the ha-postgresql chart to the namespace from above

```sh
helm -n scoreboard install postgresql-ha -f values.yaml bitnami/postgresql-ha
```

## Get pgpasswd from secret

```sh
PGPASSWD=$(kubectl get secret --namespace scoreboard postgresql-ha-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
```

## Restore data to the HA postgres cluster

Add the following to the end of the pg_dump to create a copy of the `source` schema as `source_dev`

```sql
--
-- Copy postgres DB to postgres_dev
--
CREATE DATABASE schema_dev WITH TEMPLATE schema;
```

## Restore the pg_dump

```sh
kubectl exec -it postgresql-ha-pgpool-76d85cf5d9-mwvkc -- psql postgresql://postgres:$PGPASSWD@postgresql-ha-pgpool < 00_db_init.sql
```
