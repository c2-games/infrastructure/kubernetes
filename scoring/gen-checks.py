# -*- coding: utf-8 -*-
# Generate k8s manifests for checkD
import argparse
import os
import sys
from pathlib import Path
from typing import Dict, List

from jinja2 import Template

STRATEGY_CHECK_GROUPS_TEMPLATE = Path(
    __file__, "..", "template.yml.j2"
).resolve()

primary_subnet = "wan"

adversarial_checks = [
    # default creds
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "user-root-def-creds",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_default_root_shell",
            "ssh_default_root_router",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "user-root-def-creds",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_default_root_www",
            "ssh_default_root_db",
            "ssh_default_root_dns",
        ],
    },
    # ansible login
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "user-ansible",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_ansible_www",
            "ssh_ansible_db",
            "ssh_ansible_dns",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "user-ansible",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_ansible_shell",
        ],
    },
    # nobody login
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "user-nobody",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_nobody_www",
            "ssh_nobody_db",
            "ssh_nobody_dns",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "user-nobody",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_nobody_shell",
        ],
    },
    # root malicious key
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "user-root-mal-key",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_root_malicious_key_www",
            "ssh_root_malicious_key_db",
            "ssh_root_malicious_key_dns",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "user-root-mal-key",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_root_malicious_key_router",
            "ssh_root_malicious_key_shell",
        ],
    },
    # redteam user
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "user-redteam",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_redteam_db",
            "ssh_redteam_dns",
            "ssh_redteam_www",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "user-redteam",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_redteam_shell",
        ],
    },
    # redis
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "redis",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "redis_rce_www",
            "redis_rce_db",
            "redis_rce_dns",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "redis",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "redis_rce_shell",
        ],
    },
    # malicious users
    # {
    #     "type": "adversarial",
    #     "perspective": "internal",
    #     "name": "user-malicious",
    #     "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
    #     "pull_secret": "adversarial-checks-gitlab-pull-secret",
    #     "checks": [
    #         "ssh_mal_users_www",
    #         "ssh_mal_users_db",
    #         "ssh_mal_users_dns",
    #     ],
    # },
    # {
    #     "type": "adversarial",
    #     "perspective": "external",
    #     "name": "user-malicious",
    #     "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
    #     "pull_secret": "adversarial-checks-gitlab-pull-secret",
    #     "checks": [
    #         "ssh_mal_users_shell",
    #     ],
    # },
    # www-data user
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "user-www-data",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_www_www-data_regionals",
        ],
    },
    # sshd backdoor
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "sshd-backdoor",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_sshd_backdoor_dns",
            "ssh_sshd_backdoor_www",
            "ssh_sshd_backdoor_db",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "ssh-backdoor",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "ssh_sshd_backdoor_shell",
        ],
    },
    # watershell
    {
        "type": "adversarial",
        "perspective": "internal",
        "name": "watershell",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "watershell_www",
            "watershell_db",
            "watershell_dns",
        ],
    },
    {
        "type": "adversarial",
        "perspective": "external",
        "name": "watershell",
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/adversarial-checks:latest",
        "pull_secret": "adversarial-checks-gitlab-pull-secret",
        "checks": [
            "watershell_shell",
        ],
    },
]

inject_checks = [
    {
        "type": "inject",
        "perspective": "internal",
        "name": "scoring",
        "checks": [
            "inject_scripting",
        ],
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
        "pull_secret": None,
    },
    {
        "type": "inject",
        "perspective": "internal",
        "name": "redteam",
        "checks": [
            "inject_scripting_redteam",
        ],
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
        "pull_secret": None,
    },
]

service_checks = [
    {
        "type": "service",
        "perspective": "external",
        "name": "web-icmp",
        "checks": [
            "icmp",
            "http",
            "www_content",
            "www_content_ssl",
        ],
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
        "pull_secret": None,
    },
    {
        "type": "service",
        "perspective": "external",
        "name": "dns",
        "checks": [
            "external_forward_dns",
            "external_reverse_dns",
        ],
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
        "pull_secret": None,
    },
    {
        "type": "service",
        "perspective": "external",
        "name": "ftp-ssh",
        "checks": [
            "ssh",
            "ftp_login",
            "ftp_write",
            "ftp_content",
        ],
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
        "pull_secret": None,
    },
    # {
    #     "type": "service",
    #     "perspective": "internal",
    #     "name": "ssh",
    #     "checks": [
    #         "ssh_www",
    #         "ssh_sql",
    #         "ssh_dns",
    #     ],
    #     "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
    #     "pull_secret": None,
    #     "persistent": True,
    #     "sidecar_image": "registry.gitlab.com/c2-games/scoring/service-checks/redteam-sidecar:latest",
    # },
    {
        "type": "service",
        "perspective": "internal",
        "name": "dns",
        "checks": [
            "internal_forward_dns",
            "internal_reverse_dns",
        ],
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
        "pull_secret": None,
        "persistent": True,
        "sidecar_image": "registry.gitlab.com/c2-games/scoring/service-checks/redteam-sidecar:latest",
    },
    {
        "type": "service",
        "perspective": "internal",
        "name": "sql",
        "checks": [
            "sql",
        ],
        "image": "registry.gitlab.com/c2-games/scoring/service-checks/scripts:latest",
        "pull_secret": None,
        "persistent": True,
        "sidecar_image": "registry.gitlab.com/c2-games/scoring/service-checks/redteam-sidecar:latest",
    },
]


def strategy_check_groups(teams: List[str], check_groups: List[Dict]):
    if not check_groups:
        raise ValueError("No checks specified!")
    if not teams:
        raise ValueError("No Teams specified!")

    manifests_list = []

    with open(STRATEGY_CHECK_GROUPS_TEMPLATE) as f:
        template = Template("".join(f.readlines()))

    for team in teams:
        for group in check_groups:
            # Make directory e.g. internal/service
            out_dir = (
                Path(__file__, "..", group["perspective"])
                .resolve()
                .joinpath(group["type"])
            )
            if not out_dir.exists() and args.write_out:
                os.mkdir(out_dir)
            # Filename should be e.g. external/service/Team0_ftp-ssh.yml
            out_path = out_dir.joinpath(f'{team}_{group["name"]}.yml')

            if group["perspective"] == "external":
                secondary_subnet = "wan"
            else:
                secondary_subnet = team.lower()

            manifest = template.render(
                team=team,
                check=",".join(group["checks"]),
                perspective=group["perspective"],
                check_group=group["name"],
                check_type=group["type"],
                image=group["image"],
                pull_secret=group["pull_secret"],
                primary_subnet=primary_subnet,
                secondary_subnet=secondary_subnet,
                persistent=group.get("persistent", True),
                sidecar_image=group.get("sidecar_image", None),
            )

            manifests_list.append(manifest)

            if args.write_out:
                with open(out_path, "w") as f:
                    f.write(manifest)

    return manifests_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--start-team", type=int, default=0, required=True
    )
    parser.add_argument(
        "-e", "--end-team", type=int, default=25, required=True
    )
    parser.add_argument("-A", "--adversarial", action="store_true")
    parser.add_argument("-S", "--scoring", action="store_true")
    parser.add_argument("-I", "--inject", action="store_true")
    parser.add_argument("-cp", "--check-prefix", default="", help="prefix programmatic check name. Ex, cny_24_")
    parser.add_argument("-cs", "--check-suffix", default="", help="prefix programmatic check name. Ex, _invitational")
    parser.add_argument(
        "--stdout",
        action="store_true",
        help="Write output to stdout.",
    )
    parser.add_argument(
        "--write-out", action="store_true", help="Write output to file."
    )
    args = parser.parse_args()

    teams = [f"Team{x}" for x in range(args.start_team, args.end_team + 1)]

    check_groups = []
    if args.adversarial:
        check_groups.extend(adversarial_checks)
    if args.scoring:
        check_groups.extend(service_checks)
    if args.inject:
        check_groups.extend(inject_checks)

    if args.check_prefix or args.check_suffix:
        for group in check_groups:
            group["checks"] = [args.check_prefix + check + args.check_suffix for check in group["checks"]]

    manifests = strategy_check_groups(teams, check_groups)

    if args.stdout:
        for i in manifests:
            sys.stdout.write(i)
            # Add newline separator in case template didn't have one
            sys.stdout.write("\n")
    elif args.write_out:
        print(f'generated checks for teams {",".join(teams)}')
