#!/bin/bash

user=$1
group=$2


function generateKeyCsr() {
    local user=$1
    local group=$2

    openssl req -new -newkey rsa:4096 -nodes -keyout "${user}-k8s.key" -out "${user}-k8s.csr" -subj "/CN=${user}/O=${group}"

}

function generateKubeCsr() {
    local user=$1
    local b64_csr=$2

    cat << EOF
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: ${user}-k8s-csr
spec:
  groups:
  - system:authenticated
  request: ${b64_csr}
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
EOF
}

function generateKubeConfig() {
    local user=$1
    local b64_ca=$2
    local b64_client_cert=$3
    local b64_client_key=$4

    cat<<EOF
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: $b64_ca
    server: https://k3s-master.ncaecybergames.org:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    namespace: ctf
    user: $user
  name: $user
current-context: $user
kind: Config
preferences: {}
users:
- name: $user
  user:
    client-certificate-data: $b64_client_cert
    client-key-data: $b64_client_key
EOF
}

generateKeyCsr "${user}" "${group}"

# Submit Csr to Kube API
generateKubeCsr "${user}" "$(cat "${user}-k8s.csr" | base64 | tr -d "\n")" | kubectl apply -f -

# Approve request
kubectl certificate approve "${user}-k8s-csr"

# Get Certificate
kubectl get csr "${user}-k8s-csr" -o jsonpath='{.status.certificate}' | base64 -d > "${user}-k8s.crt"

# Get CA
b64_cluster_ca=$(kubectl config view -o jsonpath='{.clusters[0].cluster.certificate-authority-data}' --raw)

generateKubeConfig "${user}" "${b64_cluster_ca}" "$(cat "${user}-k8s.crt" | base64 | tr -d '\n')" "$(cat "${user}-k8s.key" | base64 | tr -d '\n')" > "${user}-k8s.conf"
