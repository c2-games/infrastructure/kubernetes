# Provision Control Plane Nodes
resource "proxmox_vm_qemu" "k3s_controlplane" {
  for_each = {
    for index, vm in local.controlplane :
    vm.vmid => vm
  }
  name                   = each.value.name
  vmid                   = each.value.vmid
  desc                   = each.value.description
  agent                  = 1
  boot                   = "order=scsi0"
  scsihw                 = "virtio-scsi-pci"
  clone                  = each.value.source # Must be a VM Name, not a VMID
  qemu_os                = "l26"
  full_clone             = false
  memory                 = "8192" # full amount of mem available for allocation
  balloon                = "2048" # minimal amount of mem
  sockets                = 2
  cores                  = 4
  vcpus                  = 8
  cpu                    = "host"
  target_node            = var.target_node
  oncreate               = true
  onboot                 = false
  define_connection_info = false

  dynamic "disk" {
    for_each = local.controlplane_node_hardware.disks
    content {
      type    = disk.value["type"]
      size    = disk.value["size"]
      cache   = disk.value["cache"]
      backup  = disk.value["backup"]
      ssd     = disk.value["ssd"]
      discard = disk.value["discard"]
      storage = disk.value["storage"]
    }
  }

  dynamic "network" {
    for_each = local.controlplane_node_hardware.networks
    content {
      bridge    = network.value["bridge"]
      firewall  = network.value["firewall"]
      link_down = network.value["link_down"]
      model     = network.value["model"]
    }
  }
}

# Provision workers
resource "proxmox_vm_qemu" "k3s_worker" {
  for_each = {
    for index, vm in local.workers :
    vm.vmid => vm
  }
  name                   = each.value.name
  vmid                   = each.value.vmid
  desc                   = each.value.description
  agent                  = 1
  boot                   = "order=scsi0"
  scsihw                 = "virtio-scsi-pci"
  clone                  = each.value.source # Must be a VM Name, not a VMID
  qemu_os                = "l26"
  full_clone             = false
  memory                 = "8192" # full amount of mem available for allocation
  balloon                = "2048" # minimal amount of mem
  sockets                = 2
  cores                  = 4
  vcpus                  = 8
  cpu                    = "host"
  target_node            = var.target_node
  oncreate               = true
  onboot                 = false
  define_connection_info = false

  dynamic "disk" {
    for_each = local.worker_node_hardware.disks
    content {
      type    = disk.value["type"]
      size    = disk.value["size"]
      cache   = disk.value["cache"]
      backup  = disk.value["backup"]
      ssd     = disk.value["ssd"]
      discard = disk.value["discard"]
      storage = disk.value["storage"]
    }
  }

  dynamic "network" {
    for_each = local.worker_node_hardware.networks
    content {
      bridge    = network.value["bridge"]
      firewall  = network.value["firewall"]
      link_down = network.value["link_down"]
      model     = network.value["model"]
    }
  }

  # Ensure that k3s-controlplane nodes are provisioned prior to provisioning the k3s-workers
  depends_on = [
    proxmox_vm_qemu.k3s_controlplane
  ]
}