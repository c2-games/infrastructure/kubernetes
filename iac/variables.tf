variable "pm_api_url" {
  description = "Proxmox API URL"
  type        = string
  default     = null
}
variable "pm_user" {
  description = "Proxmox User"
  type        = string
  default     = null
}
variable "pm_password" {
  description = "Proxmox Password"
  type        = string
  sensitive   = true
  default     = null
}

variable "pm_api_token_id" {
  description = "Proxmox API Token ID"
  type        = string
  default     = null
}
variable "pm_api_token_secret" {
  description = "Proxmox API Token Secret"
  type        = string
  sensitive   = true
  default     = null
}

variable "target_node" {
  description = "Proxmox node to provision VMs on"
  type        = string
}

variable "num_workers" {
  description = "Number of worker nodes to provision"
  type        = string
}

variable "storage_pool" {
  description = "The storage pool to use"
  type        = string
}