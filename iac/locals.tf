locals {
  controlplane = [
    {
      vmid        = "230308101"
      name        = "k3s-controlplane01"
      description = "K3s Controlplane Node"
      source      = "k3s-controlplane01-gold"
    },
    {
      vmid        = "230308102"
      name        = "k3s-controlplane02"
      description = "K3s Controlplane Node"
      source      = "k3s-controlplane02-gold"
    },
    {
      vmid        = "230308103"
      name        = "k3s-controlplane03"
      description = "K3s Controlplane Node"
      source      = "k3s-controlplane03-gold"
    }
  ]
  controlplane_node_hardware = {
    disks = [
      {
        type    = "scsi"
        size    = "30G"
        cache   = "writethrough"
        backup  = true
        ssd     = 1
        discard = "on"
        storage = var.storage_pool
      }
    ]
    networks = [
      {
        bridge    = "vmbr667"
        firewall  = false
        link_down = false
        model     = "virtio"
      }
    ]
  }


  workers = [
    {
      vmid        = "230308111"
      name        = "k3s-worker01"
      description = "K3s Worker Node"
      source      = "k3s-worker-gold"
    },
    {
      vmid        = "230308112"
      name        = "k3s-worker02"
      description = "K3s Worker Node"
      source      = "k3s-worker-gold"
    },
    {
      vmid        = "230308113"
      name        = "k3s-worker03"
      description = "K3s Worker Node"
      source      = "k3s-worker-gold"
    }
  ]
  worker_node_hardware = {
    disks = [
      {
        type    = "scsi"
        size    = "30G"
        cache   = "writethrough"
        backup  = true
        ssd     = 1
        discard = "on"
        storage = var.storage_pool
      },
      { # scsi1
        type    = "scsi"
        size    = "1G"
        cache   = "writethrough"
        backup  = true
        ssd     = 1
        discard = "on"
        storage = var.storage_pool
      }
    ]
    networks = [
      {
        bridge    = "vmbr667"
        firewall  = false
        link_down = false
        model     = "virtio"
      },
      {
        bridge    = "vmbr3"
        firewall  = false
        link_down = false
        model     = "virtio"
      },
      {
        bridge    = "vmbr667"
        firewall  = false
        link_down = false
        model     = "virtio"
      }
    ]
  }
}