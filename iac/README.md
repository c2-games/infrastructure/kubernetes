# Scoring cluster infrastructure deployment

This README demonstrates provisioning the scoring cluster using Terraform

## Setup Access

[Setup access for the terraform proxmox provider](https://registry.terraform.io/providers/Telmate/proxmox/latest/docs#creating-the-proxmox-user-and-role-for-terraform)

## VCE1 k8s infrastructure deployment

Initialize terrform to ensure the provider is available.

```sh
terraform init
```

### Create workspaces for both `vce1` and `vce2`

```sh
terraform workspace new vce1
terraform workspace new vce2
```

#### Select workspace `vce1`

```sh
terraform workspace select vce1
```

#### Configure the runtime variables in the `vce1.tfvars` file

```ini
pm_api_url  = "https://vce1.ncaecybergames.org:8006/api2/json"
# Use an API token and token secret
pm_api_token_id = ""
pm_api_token_secret = ""
# Use a username and password if an API token isn't possible
# pm_user = "special-terraform-user@pve"
# pm_password = "a-real-strong-password"
target_node = "vce1"
storage_pool = "vce"
```

#### Run `terraform plan` plan out the infrastructure to be provisioned on VCE1

```sh
terraform plan -var-file="vce1.tfvars" -out=vce1.plan
```

<details><summary>Terraform Plan Output (Click to expand)</summary>

```txt
Terraform used the selected providers to generate the following execution plan.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # proxmox_vm_qemu.k3s_controlplane["230308101"] will be created
  + resource "proxmox_vm_qemu" "k3s_controlplane" {
      + additional_wait           = 10
      + agent                     = 1
      + automatic_reboot          = true
      + balloon                   = 2048
      + bios                      = "seabios"
      + boot                      = "order=scsi0"
      + bootdisk                  = (known after apply)
      + clone                     = "k3s-controlplane01-gold"
      + clone_wait                = 10
      + cores                     = 4
      + cpu                       = "host"
      + default_ipv4_address      = (known after apply)
      + define_connection_info    = false
      + desc                      = "K3s Controlplane Node"
      + force_create              = false
      + full_clone                = false
      + guest_agent_ready_timeout = 100
      + hotplug                   = "network,disk,usb"
      + id                        = (known after apply)
      + kvm                       = true
      + memory                    = 8192
      + name                      = "k3s-controlplane01"
      + nameserver                = (known after apply)
      + onboot                    = false
      + oncreate                  = true
      + preprovision              = true
      + qemu_os                   = "l26"
      + reboot_required           = (known after apply)
      + scsihw                    = "virtio-scsi-pci"
      + searchdomain              = (known after apply)
      + sockets                   = 2
      + ssh_host                  = (known after apply)
      + ssh_port                  = (known after apply)
      + tablet                    = true
      + target_node               = "vce1"
      + unused_disk               = (known after apply)
      + vcpus                     = 8
      + vlan                      = -1
      + vmid                      = 230308101

      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "30G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }

      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
    }

  # proxmox_vm_qemu.k3s_controlplane["230308102"] will be created
  + resource "proxmox_vm_qemu" "k3s_controlplane" {
      + additional_wait           = 10
      + agent                     = 1
      + automatic_reboot          = true
      + balloon                   = 2048
      + bios                      = "seabios"
      + boot                      = "order=scsi0"
      + bootdisk                  = (known after apply)
      + clone                     = "k3s-controlplane02-gold"
      + clone_wait                = 10
      + cores                     = 4
      + cpu                       = "host"
      + default_ipv4_address      = (known after apply)
      + define_connection_info    = false
      + desc                      = "K3s Controlplane Node"
      + force_create              = false
      + full_clone                = false
      + guest_agent_ready_timeout = 100
      + hotplug                   = "network,disk,usb"
      + id                        = (known after apply)
      + kvm                       = true
      + memory                    = 8192
      + name                      = "k3s-controlplane02"
      + nameserver                = (known after apply)
      + onboot                    = false
      + oncreate                  = true
      + preprovision              = true
      + qemu_os                   = "l26"
      + reboot_required           = (known after apply)
      + scsihw                    = "virtio-scsi-pci"
      + searchdomain              = (known after apply)
      + sockets                   = 2
      + ssh_host                  = (known after apply)
      + ssh_port                  = (known after apply)
      + tablet                    = true
      + target_node               = "vce1"
      + unused_disk               = (known after apply)
      + vcpus                     = 8
      + vlan                      = -1
      + vmid                      = 230308102

      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "30G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }

      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
    }

  # proxmox_vm_qemu.k3s_controlplane["230308103"] will be created
  + resource "proxmox_vm_qemu" "k3s_controlplane" {
      + additional_wait           = 10
      + agent                     = 1
      + automatic_reboot          = true
      + balloon                   = 2048
      + bios                      = "seabios"
      + boot                      = "order=scsi0"
      + bootdisk                  = (known after apply)
      + clone                     = "k3s-controlplane03-gold"
      + clone_wait                = 10
      + cores                     = 4
      + cpu                       = "host"
      + default_ipv4_address      = (known after apply)
      + define_connection_info    = false
      + desc                      = "K3s Controlplane Node"
      + force_create              = false
      + full_clone                = false
      + guest_agent_ready_timeout = 100
      + hotplug                   = "network,disk,usb"
      + id                        = (known after apply)
      + kvm                       = true
      + memory                    = 8192
      + name                      = "k3s-controlplane03"
      + nameserver                = (known after apply)
      + onboot                    = false
      + oncreate                  = true
      + preprovision              = true
      + qemu_os                   = "l26"
      + reboot_required           = (known after apply)
      + scsihw                    = "virtio-scsi-pci"
      + searchdomain              = (known after apply)
      + sockets                   = 2
      + ssh_host                  = (known after apply)
      + ssh_port                  = (known after apply)
      + tablet                    = true
      + target_node               = "vce1"
      + unused_disk               = (known after apply)
      + vcpus                     = 8
      + vlan                      = -1
      + vmid                      = 230308103

      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "30G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }

      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
    }

  # proxmox_vm_qemu.k3s_worker["230308111"] will be created
  + resource "proxmox_vm_qemu" "k3s_worker" {
      + additional_wait           = 10
      + agent                     = 1
      + automatic_reboot          = true
      + balloon                   = 2048
      + bios                      = "seabios"
      + boot                      = "order=scsi0"
      + bootdisk                  = (known after apply)
      + clone                     = "k3s-worker-gold"
      + clone_wait                = 10
      + cores                     = 4
      + cpu                       = "host"
      + default_ipv4_address      = (known after apply)
      + define_connection_info    = false
      + desc                      = "K3s Worker Node"
      + force_create              = false
      + full_clone                = false
      + guest_agent_ready_timeout = 100
      + hotplug                   = "network,disk,usb"
      + id                        = (known after apply)
      + kvm                       = true
      + memory                    = 8192
      + name                      = "k3s-worker01"
      + nameserver                = (known after apply)
      + onboot                    = false
      + oncreate                  = true
      + preprovision              = true
      + qemu_os                   = "l26"
      + reboot_required           = (known after apply)
      + scsihw                    = "virtio-scsi-pci"
      + searchdomain              = (known after apply)
      + sockets                   = 2
      + ssh_host                  = (known after apply)
      + ssh_port                  = (known after apply)
      + tablet                    = true
      + target_node               = "vce1"
      + unused_disk               = (known after apply)
      + vcpus                     = 8
      + vlan                      = -1
      + vmid                      = 230308111

      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "30G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }
      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "1G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }

      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
      + network {
          + bridge    = "vmbr3"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
    }

  # proxmox_vm_qemu.k3s_worker["230308112"] will be created
  + resource "proxmox_vm_qemu" "k3s_worker" {
      + additional_wait           = 10
      + agent                     = 1
      + automatic_reboot          = true
      + balloon                   = 2048
      + bios                      = "seabios"
      + boot                      = "order=scsi0"
      + bootdisk                  = (known after apply)
      + clone                     = "k3s-worker-gold"
      + clone_wait                = 10
      + cores                     = 4
      + cpu                       = "host"
      + default_ipv4_address      = (known after apply)
      + define_connection_info    = false
      + desc                      = "K3s Worker Node"
      + force_create              = false
      + full_clone                = false
      + guest_agent_ready_timeout = 100
      + hotplug                   = "network,disk,usb"
      + id                        = (known after apply)
      + kvm                       = true
      + memory                    = 8192
      + name                      = "k3s-worker02"
      + nameserver                = (known after apply)
      + onboot                    = false
      + oncreate                  = true
      + preprovision              = true
      + qemu_os                   = "l26"
      + reboot_required           = (known after apply)
      + scsihw                    = "virtio-scsi-pci"
      + searchdomain              = (known after apply)
      + sockets                   = 2
      + ssh_host                  = (known after apply)
      + ssh_port                  = (known after apply)
      + tablet                    = true
      + target_node               = "vce1"
      + unused_disk               = (known after apply)
      + vcpus                     = 8
      + vlan                      = -1
      + vmid                      = 230308112

      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "30G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }
      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "1G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }

      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
      + network {
          + bridge    = "vmbr3"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
    }

  # proxmox_vm_qemu.k3s_worker["230308113"] will be created
  + resource "proxmox_vm_qemu" "k3s_worker" {
      + additional_wait           = 10
      + agent                     = 1
      + automatic_reboot          = true
      + balloon                   = 2048
      + bios                      = "seabios"
      + boot                      = "order=scsi0"
      + bootdisk                  = (known after apply)
      + clone                     = "k3s-worker-gold"
      + clone_wait                = 10
      + cores                     = 4
      + cpu                       = "host"
      + default_ipv4_address      = (known after apply)
      + define_connection_info    = false
      + desc                      = "K3s Worker Node"
      + force_create              = false
      + full_clone                = false
      + guest_agent_ready_timeout = 100
      + hotplug                   = "network,disk,usb"
      + id                        = (known after apply)
      + kvm                       = true
      + memory                    = 8192
      + name                      = "k3s-worker03"
      + nameserver                = (known after apply)
      + onboot                    = false
      + oncreate                  = true
      + preprovision              = true
      + qemu_os                   = "l26"
      + reboot_required           = (known after apply)
      + scsihw                    = "virtio-scsi-pci"
      + searchdomain              = (known after apply)
      + sockets                   = 2
      + ssh_host                  = (known after apply)
      + ssh_port                  = (known after apply)
      + tablet                    = true
      + target_node               = "vce1"
      + unused_disk               = (known after apply)
      + vcpus                     = 8
      + vlan                      = -1
      + vmid                      = 230308113

      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "30G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }
      + disk {
          + backup             = true
          + cache              = "writethrough"
          + discard            = "on"
          + file               = (known after apply)
          + format             = (known after apply)
          + iops               = 0
          + iops_max           = 0
          + iops_max_length    = 0
          + iops_rd            = 0
          + iops_rd_max        = 0
          + iops_rd_max_length = 0
          + iops_wr            = 0
          + iops_wr_max        = 0
          + iops_wr_max_length = 0
          + iothread           = 0
          + mbps               = 0
          + mbps_rd            = 0
          + mbps_rd_max        = 0
          + mbps_wr            = 0
          + mbps_wr_max        = 0
          + media              = (known after apply)
          + replicate          = 0
          + size               = "1G"
          + slot               = (known after apply)
          + ssd                = 1
          + storage            = "vce"
          + storage_type       = (known after apply)
          + type               = "scsi"
          + volume             = (known after apply)
        }

      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
      + network {
          + bridge    = "vmbr3"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
      + network {
          + bridge    = "vmbr667"
          + firewall  = false
          + link_down = false
          + macaddr   = (known after apply)
          + model     = "virtio"
          + queues    = (known after apply)
          + rate      = (known after apply)
          + tag       = -1
        }
    }

Plan: 6 to add, 0 to change, 0 to destroy.

──────────────────────────────────────────

Saved the plan to: vce1.plan

To perform exactly these actions, run the following command to apply:
    terraform apply "vce1.plan"

```

</details>

#### Apply the infrastructure planned above to VCE1

```sh
terraform apply vce1.plan
```

<details><summary>Terraform Apply Output (Click to expand)</summary>

```txt
proxmox_vm_qemu.k3s_controlplane["230308102"]: Creating...
proxmox_vm_qemu.k3s_controlplane["230308103"]: Creating...
proxmox_vm_qemu.k3s_controlplane["230308101"]: Creating...
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Creation complete after 1m10s [id=vce1/qemu/230308103]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Creation complete after 1m10s [id=vce1/qemu/230308102]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Creation complete after 1m10s [id=vce1/qemu/230308101]
proxmox_vm_qemu.k3s_worker["230308111"]: Creating...
proxmox_vm_qemu.k3s_worker["230308112"]: Creating...
proxmox_vm_qemu.k3s_worker["230308113"]: Creating...
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m20s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m20s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m20s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m30s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m30s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m30s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m40s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m40s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m40s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Creation complete after 1m48s [id=vce1/qemu/230308111]
proxmox_vm_qemu.k3s_worker["230308113"]: Creation complete after 1m48s [id=vce1/qemu/230308113]
proxmox_vm_qemu.k3s_worker["230308112"]: Creation complete after 1m48s [id=vce1/qemu/230308112]

Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```

</details>

---

## VCE2 k8s infrastructure deployment

Rinse and repeat the steps from VCE1 for the VCE2 Infrastructure.

### Select workspace `vce2`

```sh
terraform workspace select vce2
```

### Configure the runtime variables in the `vce2.tfvars` file

```ini
pm_api_url  = "https://vce2.ncaecybergames.org::8006/api2/json"
pm_api_token_id = ""
pm_api_token_secret = ""
target_node = "vce2"
storage_pool = "vce"
```

### Run `terraform plan` plan out the infrastructure to be provisioned on VCE2

```sh
terraform plan -var-file="vce2.tfvars" -out=vce2.plan
```

*`terraform plan` output skipped for brevity*

### Apply the infrastructure planned above to VCE2

```sh
terraform apply vce2.plan
```

<details><summary>Terraform Apply Output (Click to expand)</summary>

```txt
proxmox_vm_qemu.k3s_controlplane["230308102"]: Creating...
proxmox_vm_qemu.k3s_controlplane["230308103"]: Creating...
proxmox_vm_qemu.k3s_controlplane["230308101"]: Creating...
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308103"]: Creation complete after 1m10s [id=vce2/qemu/230308103]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_controlplane["230308102"]: Creation complete after 1m10s [id=vce2/qemu/230308102]
proxmox_vm_qemu.k3s_controlplane["230308101"]: Creation complete after 1m10s [id=vce2/qemu/230308101]
proxmox_vm_qemu.k3s_worker["230308111"]: Creating...
proxmox_vm_qemu.k3s_worker["230308112"]: Creating...
proxmox_vm_qemu.k3s_worker["230308113"]: Creating...
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [10s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [20s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [30s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [40s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [50s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m0s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m10s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m20s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m20s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m20s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m30s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m30s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m30s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Still creating... [1m40s elapsed]
proxmox_vm_qemu.k3s_worker["230308113"]: Still creating... [1m40s elapsed]
proxmox_vm_qemu.k3s_worker["230308112"]: Still creating... [1m40s elapsed]
proxmox_vm_qemu.k3s_worker["230308111"]: Creation complete after 1m48s [id=vce2/qemu/230308111]
proxmox_vm_qemu.k3s_worker["230308113"]: Creation complete after 1m48s [id=vce2/qemu/230308113]
proxmox_vm_qemu.k3s_worker["230308112"]: Creation complete after 1m48s [id=vce2/qemu/230308112]

Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```

</details>

---

## Troubleshooting

If an `HTTP/599` error is experienced, manually run the `terraform apply`
command again.

*Note that the following command will perform a plan followed by a query as to
 whether or not you would like to proceed with the application.*

```sh
terraform apply -var-file="{{failing_environment_name}}.tfvars"
```
